package main

import (
	"fmt"
	"github.com/go-echarts/go-echarts/charts"
	"math"
	"os"
	"sort"
	"time"
)

func GetTimeCalculatingAreaOLD(shapes *Shapes, sizeDataset int) time.Duration {
	startTime := time.Now()

	for i := 0; i < sizeDataset; i++ {
		ShapeObj := GetShapeObj(&shapes.Shapes[i])
		ShapeObj.Area()
	}

	return time.Since(startTime)
}

func GetTimeCalculatingAreaNEW(shapes *Shapes, gorQTY int, gorCap int) time.Duration {
	startTime := time.Now()

	for i := 0; i < gorQTY; i++ {
		go CalculateAreaPartly(shapes, i, gorCap)
	}

	return time.Since(startTime)
}

// A function used by goroutines to calculate the area of a specific parts of shapes dataset
func CalculateAreaPartly(shapes *Shapes, gorNum int, gorCap int) {
	partitionStart := gorNum*gorCap
	for i := 0; i < gorCap; i++ {
		ShapeObj := GetShapeObj(&shapes.Shapes[partitionStart + i])
		ShapeObj.Area() // Calculation the shape area. Finally.
	}
}

// Returns an array of divisors
func GetDivisors(n int) (divisorsArr []int) {
	for i := 1; i < int(math.Sqrt(float64(n)) + 1); i++{
		if n % i == 0 {
			divisorsArr = append(divisorsArr, i)
			if n / i != i {
				divisorsArr = append(divisorsArr, n / i)
			}
		}
	}
	sort.Ints(divisorsArr)
	return divisorsArr
}

// Builds a chart and saves the result to a file Comparison-?.html
func BuildGraph(allGorNums *[]int, allElapsedTimesNEW *[]int64, allElapsedTimesOLD *[]int64, sizeDataset int) {
	line := charts.NewLine()

	line.SetGlobalOptions(
		charts.TitleOpts{Title: "Sequential vs Concurrent"},
		charts.XAxisOpts{Name: "Goroutines"},
		charts.YAxisOpts{Name: "Nanoseconds"},
	)

	line.AddXAxis(allGorNums).
		AddYAxis("Concurrent", allElapsedTimesNEW).
		AddYAxis("Sequential", allElapsedTimesOLD)

	_ = os.Mkdir("result", 0700)
	fileName := fmt.Sprintf("result/Comparison-%d.html", sizeDataset)
	f, err := os.Create(fileName)
	if err != nil {
		panic(err)
	}
	line.Render(f)
	fmt.Printf("\n%s was created.\nYou can explore data visualization by opening this file.\n\n", fileName)
}