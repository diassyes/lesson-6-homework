package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

func main() {
	sizeDataset := 1000000
	var shapes Shapes
	var timeElapsedNEW time.Duration
	var timeElapsedOLD time.Duration
	var allElapsedTimesNEW []int64
	var allElapsedTimesOLD []int64
	var allGorQTY []int

	// ------------ Generating, Reading and Unmarshalling JSON
	_ = os.Mkdir("dataset", 0700)
	filenameJSON := fmt.Sprintf("dataset/shapes-%d.json", sizeDataset)
	dataJsonPtr, err := ioutil.ReadFile(filenameJSON)
	if err != nil {
		GenerateShapesJSON(sizeDataset, filenameJSON)
		dataJsonPtr, _ = ioutil.ReadFile(filenameJSON)
		_ = json.Unmarshal(dataJsonPtr, &shapes)
	} else {
		_ = json.Unmarshal(dataJsonPtr, &shapes)
		sizeDataset = len(shapes.Shapes)
	}
	fmt.Println(sizeDataset, "- Dataset size")
	// ------------ Generating, Reading and Unmarshalling JSON

	// ------------ Calculating the area using different goroutines range
	// gorQTY - goroutines quantity (divisor of sizeDataset)
	// gorCap - goroutine capacity
	// Example:
	// 		sizeDataset: 100, gorQTY: 4, gorCap: 25;
	// 		sizeDataset: 100000, gorQTY: 125, gorCap: 800;
	for _, gorQTY := range GetDivisors(sizeDataset) {
		gorCap := sizeDataset / gorQTY
		timeElapsedNEW = GetTimeCalculatingAreaNEW(&shapes, gorQTY, gorCap)
		allGorQTY = append(allGorQTY, gorQTY)
		allElapsedTimesNEW = append(allElapsedTimesNEW, timeElapsedNEW.Nanoseconds())
		fmt.Printf("New: %s / %d goroutines\n", timeElapsedNEW, gorQTY)
	}
	// ------------ Calculating the area using different goroutines range

	// ------------ Calculating the area sequential
	timeElapsedOLD = GetTimeCalculatingAreaOLD(&shapes, sizeDataset)
	for range allElapsedTimesNEW {
		allElapsedTimesOLD = append(allElapsedTimesOLD, timeElapsedOLD.Nanoseconds())
	}
	fmt.Println("\nOld: ", timeElapsedOLD)
	// ------------ Calculating the area sequential

	BuildGraph(&allGorQTY, &allElapsedTimesNEW, &allElapsedTimesOLD, sizeDataset)
}